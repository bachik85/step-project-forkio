const gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    prefix = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    minifyJS = require('gulp-js-minify'),
    uglifyJS = require('gulp-uglify');


const buildCSS = () =>
    gulp.src('./src/scss/*.scss')
        .pipe(sass())
        .pipe(concat('style.min.css'))
        .pipe(prefix({cascade: false}))
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('./public/css/'));

const buildJS = () =>
    gulp.src('./src/js/**/*.js')
        .pipe(concat('script.min.js'))
        .pipe(minifyJS())
        .pipe(uglifyJS())
        .pipe(gulp.dest('./public/js/'));

const buildHTML = () =>
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./public/'));

const buildIMG = () =>
    gulp.src('./src/img/**/*.{jpg,png,svg,gif,webp}')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img/'));

const watch = () => {
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });
    gulp.watch('./src/scss/**/*.scss', buildCSS);
    gulp.watch('./src/index.html', buildHTML);
    gulp.watch('./src/js/**/*.js', buildJS);
    gulp.watch('./src/img/**/*.{jpg,png,svg,gif,webp}', buildIMG);
    gulp.watch('./src/**/*.*').on('change', browserSync.reload);
}

gulp.task('build', gulp.series(
    buildCSS,
    buildHTML,
    buildJS,
    buildIMG
));
gulp.task('dev', gulp.series("build", watch));