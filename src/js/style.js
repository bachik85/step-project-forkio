const burgerButton = document.getElementById("burgerButton"),
    menuWrapper = document.getElementById("menuWrapper");

burgerButton.addEventListener("click", () => {
        burgerButton.classList.toggle("burger-is-active");
        menuWrapper.classList.toggle("menu-is-active");
});

document.addEventListener("click", e => {
    if(menuWrapper.classList.contains("menu-is-active")) {
        if (!menuWrapper.contains(e.target) && !burgerButton.contains(e.target)) {
            burgerButton.classList.toggle("burger-is-active");
            menuWrapper.classList.toggle("menu-is-active");
        }
    }
});
