# Step-Project-Forkio

Artem Kopynets <br>
Artem Ponomarenko <br>

##Features:
 
* git<br>
* preprocess sass/scss<br>
* task-manager gulp<br>
* convert *.scss to *.css<br>
* autoreloader<br>
* minify different file extention<br>
* clean district folder<br>
* autoprefix css properties<br>


##Tasks:

Student Artem Ponomarenko:<br>

* Set up project build using Gulp collector.<br>
* Lay out the site header with the top menu (including drop-down menu at low screen resolutions).<br>
* Make a section People Are Talking About Fork.<br>

Student Artem Kopynets:<br>

* Set up project build using Gulp collector.<br>
* Make a block Revolutionary Editor.<br>
* Make a section Here is what you get.<br>
* Lay down the Fork Subscription Pricing section. In the block with pricing, the third element will always be “allocated” and will be more than others.<br>
